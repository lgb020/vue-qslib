import Vue from 'vue';
import BaseOption from './BaseOption';
import UiCommands from './UiCommands';
import QuestionSubject from './QuestionSubject';
import DataDefine, { QuestionDataTypes } from './QuestionDataTypes';
import OrderBy, {OrderByTypes} from './OrderByTypes';
import GUID from './GUID';

export default abstract class BaseQuestion<T extends BaseOption> extends Vue {





    public Data: QuestionSubject<T> = new QuestionSubject<T>();


    public canEdit: boolean = true;

    public OrderByItems: OrderBy[] = [];


    constructor(_title: string) {
        super();

        this.OrderByItems.push(new OrderBy('纵向排列', OrderByTypes.obtVertical));
        this.OrderByItems.push(new OrderBy('横行向排列', OrderByTypes.obtHorizontal));


    }
    public created() {

     console.info('init data while mounted');
    }




    // 获取题目数据
    public abstract getQuestionData(): string;
    // 设置题目数据
    public abstract setQuestionData(data: string): void;

    public abstract insert(id: string): void;
    // 添加新的选项
    public createNewOption(c: new() => T): T {
        const options = this.Data.Options;
        const opt = new c();
        opt.itemId = new GUID().toString();
        opt.itemName = '选项' + options.length;

        return opt;
    }

    public createTObject<TT>(c: new() => TT): TT {
        const obj = new c();
        return obj;
    }

    // 移除选项
    public remove(id: string): void {
        const options = this.Data.Options;
        options.splice(options.findIndex((item) => item.itemId === id), 1);
    }
    // 选项上移动
    public moveUp(id: string): void {
        const options = this.Data.Options;
        const index = options.findIndex((item) => item.itemId === id);

           if (index === 0) {
        return;
           }

           const upObj = options[index - 1];
           const moveObj = options[index];
           options.splice(index, 1, upObj); // (索引，长度，值)
           options.splice(index - 1, 1, moveObj); // (索引，长度，值)
    }

    // 选项向下移动
    public moveDown(id: string): void {
        const options = this.Data.Options;
        const index = options.findIndex((item) => item.itemId === id);



           if (index === (options.length - 1)) {
        return;
           }

           const downObj = options[index + 1];
           const moveObj = options[index];
           options.splice(index, 1, downObj); // (索引，长度，值)
           options.splice(index + 1, 1, moveObj); // (索引，长度，值)
    }
    public hasGotType(type: string): void {

    }

    public setEdit(): void {
        this.canEdit = !this.canEdit;

    }
    public SetTitle(_title: string): void {
        this.Data.Title = _title;
    }
    public setCodeNum(code: string): void {
        this.Data.CodeNum = code;
    }

    public setQuestionId(qId: string): void {
        this.Data.Id = qId;
    }

    public setSelValue(value: string): void {
        this.Data.SelValue = value;
    }

    public setDataType(dataType: QuestionDataTypes) {
       this.Data.DataType = dataType;
    }



}
